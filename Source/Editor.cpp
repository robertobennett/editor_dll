#include "stdafx.h"
#include "Editor.h"

CEditor::CEditor() :
m_pEngine(nullptr),
m_bInitialized(false)
{
	return;
}

CEditor::~CEditor()
{
	delete m_pEngine;
	m_pEngine = nullptr;

	m_bInitialized = false;
	return;
}

bool CEditor::Init(HWND hRenderTarget, UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if (!m_bInitialized && hRenderTarget != NULL)
	{
		m_hRenderTarget = hRenderTarget;

		m_pEngine = new CEngine();

		if (!m_pEngine->Init(hRenderTarget, iRenderTargetWidth, iRenderTargetHeight))
			return false;
		
		m_bInitialized = true;
		return true;
	}

	return false;
}

void CEditor::Frame()
{
	m_pEngine->Frame();

	return;
}

void CEditor::Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if (m_pEngine != nullptr)
		m_pEngine->Resize(iRenderTargetWidth, iRenderTargetHeight);

	return;
}

HWND CEditor::WndProc(HWND hWnd, int iMsg, WPARAM wParam, LPARAM lParam)
{
	if (hWnd == m_hRenderTarget)
	{
		switch ((UINT)iMsg)
		{
			default:
				break;

			case WM_PAINT:
				m_pEngine->Frame();
				break;
		}
	}

	return m_hRenderTarget;
}