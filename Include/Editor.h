#pragma once
#ifndef EDITOR_DLL_EDITOR_H
#define EDITOR_DLL_EDITOR_H

#include <Engine.h>

class CEditor
{
	public:
		DLL_IMPORT_EXPORT CEditor();
		DLL_IMPORT_EXPORT ~CEditor();

		bool DLL_IMPORT_EXPORT Init(HWND hRenderTarget, UINT iRenderTargetWidth, UINT iRenderTargetHeight);
		void DLL_IMPORT_EXPORT Frame();
		void DLL_IMPORT_EXPORT Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight);
		HWND DLL_IMPORT_EXPORT WndProc(HWND hWnd, int iMsg, WPARAM wParam, LPARAM lParam);

	private:
		HWND m_hRenderTarget;
		bool m_bInitialized;

		CEngine * m_pEngine;
};

#endif